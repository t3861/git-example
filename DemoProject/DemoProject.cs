﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoProject
{
    public partial class DemoProject : Form
    {
        public DemoProject()
        {
            InitializeComponent();
        }

        private void btnClickThis_Click(object sender, EventArgs e)
        {
            lblHelloWorld.Text = "Hello World!";
        }

        private int c = 0;

        private void CountButton_Click(object sender, EventArgs e)
        {
            c++;
            label1.Text = c.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            switch (button3.BackColor.Name)
            {
                case "Red":
                    button3.BackColor = Color.Yellow;
                    break;
                case "Black":
                    button3.BackColor = Color.White;
                    break;
                case "White":
                    button3.BackColor = Color.Red;
                    break;
                case "Yellow":
                    button3.BackColor = Color.Purple;
                    break;
                default:
                    button3.BackColor = Color.Red;
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
